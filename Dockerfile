FROM node:13 AS builder
WORKDIR /app
COPY ./package.json ./
RUN yarn
COPY . .
RUN yarn build

FROM node:13
WORKDIR /app
COPY --from=builder /app ./
EXPOSE 5000
ENTRYPOINT ["yarn", "start:prod"]
