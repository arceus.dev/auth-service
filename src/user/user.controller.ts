import {Controller} from '@nestjs/common';
import { GrpcMethod, RpcException } from '@nestjs/microservices';
import { User } from '../entity/User';

class UserById {
    id: string;
}

@Controller('user')
export class UserController {
    @GrpcMethod('AuthService', 'UserById')
    async userById(data: UserById): Promise<User> {
        const user = await User.findOne({where: {id: data.id}});

        if(user) {
            return user;
        } else {
            throw new RpcException('Not found');
        }
    }
}
