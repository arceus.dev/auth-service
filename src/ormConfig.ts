import * as path from "path";
import { ConnectionOptions } from 'typeorm/index';

const config: ConnectionOptions = {
  type: 'mysql',
  host: 'mysql-service',
  port: 3306,
  username: 'root',
  password: 'password',
  database: 'damien',
  synchronize: false,
  migrationsRun: false,
  entities: [path.resolve(__dirname, 'entity/**.*')],
  migrations: [path.resolve(__dirname, 'migration/**')],
  cli: {
    entitiesDir: 'src/entity',
    migrationsDir: 'src/migration',
  },
}

export = config;
