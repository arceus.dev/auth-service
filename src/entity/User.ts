import {Entity} from "typeorm";
import { BaseEntity, Column, PrimaryGeneratedColumn } from 'typeorm/index';

@Entity()
export class User extends BaseEntity{
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column()
  firstname: string;

  @Column()
  lastname: string;
}
